package com.melnyk.creator;

import com.melnyk.cars.Bus;
import com.melnyk.cars.Car;
import com.melnyk.cars.PassengerCar;
import com.melnyk.cars.Truck;
import java.util.ArrayList;
import java.util.List;

public class CarsCreator {

  private List<Car> cars;

  public CarsCreator() {
    this.cars = createCars();
  }

  public final List<Car> getCars() {
    return cars;
  }

  private List<Car> createCars() {
    cars = new ArrayList<Car>();
    cars.add(new Truck("Volvo", "FH 2015", "Swedish",
        150, 3, true));
    cars.add(new Bus("Volvo", "FH 2015", "Swedish",
        150, 2, 40));
    cars.add(new Truck("Mercedes-Benz", "Sprinter 313", "Germany",
        180, 3, false));
    cars.add(new Truck("Mercedes-Benz", "814", "Germany",
        150, 3, false));
    cars.add(new Truck("MAN", "TGA 360", "Germany",
        150, 3, true));
    cars.add(new PassengerCar("Volkswagen", "Golf", "Germany",
        150, 3));
    cars.add(new PassengerCar("Volkswagen", "Polo", "Germany",
        150, 3));
    cars.add(new PassengerCar("Volkswagen", "Tiguan", "Germany",
        150, 3));
    cars.add(new PassengerCar("Volkswagen", "Passat", "Germany",
        150, 3));
    cars.add(new PassengerCar("BMW", "I8", "Germany",
        220, 2));
    cars.add(new PassengerCar("BMW", "I3", "Germany",
        120, 5));
    cars.add(new PassengerCar("BMW", "X6", "Germany",
        150, 5));
    cars.add(new PassengerCar("BMW", "X7", "Germany",
        180, 5));
    cars.add(new PassengerCar("Toyota", "X6", "Japan",
        180, 5));
    cars.add(new PassengerCar("Toyota", "X6", "Japan",
        165, 5));
    cars.add(new Bus("Mercedes-Benz", "Sprinter 316", "Germany",
        150, 3, 28));
    return cars;
  }
}
