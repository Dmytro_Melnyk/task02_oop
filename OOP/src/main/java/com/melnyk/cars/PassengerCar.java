package com.melnyk.cars;

public class PassengerCar extends Car {

  public PassengerCar(final String mark, final String model,
      final String countryProducer, final int maxSpeed, final int seats) {
    super(mark, model, countryProducer, maxSpeed, seats);
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "PassengerCar: " + super.toString();
  }
}
