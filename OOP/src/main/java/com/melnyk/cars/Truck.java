package com.melnyk.cars;

public class Truck extends Car {

  private boolean trailer;

  public Truck(final String mark, final String model,
      final String countryProducer, final int maxSpeed,
      final int seats, final boolean trailer) {
    super(mark, model, countryProducer, maxSpeed, seats);
    this.trailer = trailer;
  }

  public final boolean isTrailer() {
    return trailer;
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "Truck: " + super.toString()
        + ", trailer=" + trailer;
  }
}
