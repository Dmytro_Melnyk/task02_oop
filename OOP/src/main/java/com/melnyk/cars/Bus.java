package com.melnyk.cars;

public class Bus extends Car {

  private int additionalSeats;

  public Bus(final String mark, final String model,
      final String countryProducer, final int maxSpeed,
      final int seats, final int additionalSeats) {
    super(mark, model, countryProducer, maxSpeed, seats);
    this.additionalSeats = additionalSeats;
  }

  public final int getAdditionalSeats() {
    return additionalSeats;
  }

  @Override
  public final void showParameters() {
    System.out.println(toString());
  }

  @Override
  public final String toString() {
    return "Bus: " + super.toString()
        + ", additionalSeats=" + additionalSeats;
  }
}
