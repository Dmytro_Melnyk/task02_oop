package com.melnyk.cars;

public abstract class Car {

  private String mark;
  private String model;
  private String countryProducer;
  private int maxSpeed;
  private int seats;

  public Car(final String mark, final String model,
      final String countryProducer, final int maxSpeed, final int seats) {
    this.mark = mark;
    this.model = model;
    this.countryProducer = countryProducer;
    this.maxSpeed = maxSpeed;
    this.seats = seats;
  }

  public final String getMark() {
    return mark;
  }

  public final String getModel() {
    return model;
  }

  public final String getCountryProducer() {
    return countryProducer;
  }

  public final int getMaxSpeed() {
    return maxSpeed;
  }

  public final int getSeats() {
    return seats;
  }

  public abstract void showParameters();

  @Override
  public String toString() {
    return " mark: " + mark
        + ", model: " + model
        + ", countryProducer: " + countryProducer
        + ", maxSpeed: " + maxSpeed
        + ", seats: " + seats;
  }
}
