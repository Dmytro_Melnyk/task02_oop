package com.melnyk.menu;

import com.melnyk.filters.Service;
import java.util.Scanner;

public class Menu {

  private Service service;

  public Menu() {
    service = new Service();
  }

  public final void menu() {
    boolean check = false;
    Scanner choice = new Scanner(System.in);
    do {
      service.showMenu();
      int show = choice.nextInt();
      switch (show) {
        case 1:
          service.viewAllCars();
          break;
        case 2:
          service.sortByType();
          break;
        case 3:
          service.sortByMark();
          break;
        case 4:
          service.sortByCountryProducer();
          break;
        case 5:
          service.searchByMaxSpeen();
          break;
        case 6:
          check = true;
          break;
        default:
          break;
      }
    } while (!check);
  }
}
