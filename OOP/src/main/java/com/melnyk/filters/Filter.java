package com.melnyk.filters;

import com.melnyk.cars.Bus;
import com.melnyk.cars.Car;
import com.melnyk.cars.PassengerCar;
import com.melnyk.cars.Truck;
import com.melnyk.creator.CarsCreator;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Filter {

  private List<Car> cars;
  private CarsCreator carsCreator;

  public final void setCars(final List<Car> cars) {
    this.cars = cars;
  }

  public Filter() {
    carsCreator = new CarsCreator();
  }

  final List<Car> getAllCars() {
    cars = null;
    return carsCreator.getCars();
  }

  public final List<Car> filterByCountryProducer(final String countryProducer) {
    if (cars == null) {
      cars = carsCreator.getCars().stream()
          .filter(car -> car.getCountryProducer().equals(countryProducer))
          .sorted(Comparator.comparing(Car::getCountryProducer))
          .collect(Collectors.toList());
    } else {
      cars = cars.stream()
          .filter(car -> car.getCountryProducer().equals(countryProducer))
          .sorted(Comparator.comparing(Car::getCountryProducer))
          .collect(Collectors.toList());
    }
    return cars;
  }

  public final List<Car> filterByMark(final String mark) {
    if (cars == null) {
      cars = carsCreator.getCars().stream()
          .filter(car -> car.getMark().equals(mark))
          .sorted(Comparator.comparing(Car::getMark))
          .collect(Collectors.toList());
    } else {
      cars = cars.stream().filter(car -> car.getMark().equals(mark))
          .sorted(Comparator.comparing(Car::getMark))
          .collect(Collectors.toList());
    }
    return cars;
  }

  public final List<Car> filterByMaxSpeed(final int maxSpeed) {
    if (cars == null) {
      cars = carsCreator.getCars().stream()
          .filter(car -> car.getMaxSpeed() >= maxSpeed)
          .sorted(Comparator.comparingInt(Car::getMaxSpeed))
          .collect(Collectors.toList());
    } else {
      cars = cars.stream()
          .filter(car -> car.getMaxSpeed() >= maxSpeed)
          .sorted(Comparator.comparingInt(Car::getMaxSpeed))
          .collect(Collectors.toList());
    }
    return cars;
  }

  public final List<Car> filterByType(final String type) {
    if (cars == null) {
      switch (type) {
        case "Bus":
          cars = carsCreator.getCars().stream()
              .filter(car -> car instanceof Bus)
              .collect(Collectors.toList());
          break;
        case "Truck":
          cars = carsCreator.getCars().stream()
              .filter(car -> car instanceof Truck)
              .collect(Collectors.toList());
          break;
        default:
          cars = carsCreator.getCars().stream()
              .filter(car -> car instanceof PassengerCar)
              .collect(Collectors.toList());
          break;
      }
    } else {
      switch (type) {
        case "Bus":
          cars = cars.stream()
              .filter(car -> car instanceof Bus)
              .collect(Collectors.toList());
          break;
        case "Truck":
          cars = cars.stream()
              .filter(car -> car instanceof Truck)
              .collect(Collectors.toList());
          break;
        default:
          cars = cars.stream()
              .filter(car -> car instanceof PassengerCar)
              .collect(Collectors.toList());
          break;
      }
    }
    return cars;
  }
}
