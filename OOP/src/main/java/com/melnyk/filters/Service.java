package com.melnyk.filters;

import com.melnyk.cars.Car;
import java.util.Scanner;

public class Service {

  private Filter filter;

  public Service() {
    filter = new Filter();
  }

  public final void showMenu() {
    System.out.print("1. View all cars.\n"
        + "2. Sort by type.\n"
        + "3. Sort by mark.\n"
        + "4. Sort by country producer.\n"
        + "5. Search by max speed.\n"
        + "6. Exit\n"
        + "Make your choice: ");
  }

  public final void viewAllCars() {
    for (Car car : filter.getAllCars()) {
      System.out.println(car);
    }
  }

  public final void sortByType() {
    System.out.print("\nEnter type of car:\n"
        + "1. PassengerCar\n"
        + "2. Truck\n"
        + "3. Bus\n");
    Scanner choice = new Scanner(System.in);
    int typeNumber = choice.nextInt();
    switch (typeNumber) {
      case 1:
        for (Car car : filter.filterByType("PassengerCar")) {
          System.out.println(car);
        }
        break;
      case 2:
        for (Car car : filter.filterByType("Truck")) {
          System.out.println(car);
        }
        break;
      default:
        for (Car car : filter.filterByType("Bus")) {
          System.out.println(car);
        }
        break;
    }
  }

  public final void sortByMark() {
    System.out.print("\nEnter mark of car:\n"
        + "1. Toyota\n"
        + "2. Mercedes-Benz\n"
        + "3. MAN\n"
        + "4. BMW\n");
    Scanner choice = new Scanner(System.in);
    int markNumber = choice.nextInt();
    switch (markNumber) {
      case 1:
        for (Car car : filter.filterByMark("Toyota")) {
          System.out.println(car);
        }
        break;
      case 2:
        for (Car car : filter.filterByMark("Mercedes-Benz")) {
          System.out.println(car);
        }
        break;
      case 3:
        for (Car car : filter.filterByMark("MAN")) {
          System.out.println(car);
        }
        break;
      case 4:
        for (Car car : filter.filterByMark("BMW")) {
          System.out.println(car);
        }
        break;
      default:
        System.out.println("There are no cars from this country");
        break;
    }
  }

  public final void sortByCountryProducer() {
    System.out.print("\nEnter name of country: ");
    Scanner country = new Scanner(System.in);
    String countryProducer = country.nextLine();
    switch (countryProducer) {
      case "Germany":
        for (Car car : filter.filterByCountryProducer("Germany")) {
          System.out.println(car);
        }
        break;
      case "Japan":
        for (Car car : filter.filterByCountryProducer("Japan")) {
          System.out.println(car);
        }
        break;
      case "Swedish":
        for (Car car : filter.filterByCountryProducer("Swedish")) {
          System.out.println(car);
        }
        break;
      default:
        System.out.println("There are no cars from this country!");
        break;
    }
  }

  public final void searchByMaxSpeen() {
    System.out.print("\nEnter max speed of car:\n");
    Scanner choice = new Scanner(System.in);
    int maxSpeed = choice.nextInt();
    if (filter.filterByMaxSpeed(maxSpeed).isEmpty()) {
      System.out.println("There are no car with such speed!");
      filter.setCars(null);
    } else {
      for (Car car : filter
          .filterByMaxSpeed(maxSpeed)) {
        System.out.println(car);
      }
    }
  }
}
